<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');


Route::get('/register', 'AuthController@register');

Route::post('/home', 'AuthController@kirim');
Route::get('/data-tabel',function(){
    return view('tabel.data-tabel');
});



Route::group(['middleware' => ['auth']], function () {
    Route::get('/cast/create','CastController@create');
    Route::post('/cast','CastController@store');
    Route::get('/cast','CastController@index');
    Route::get('/cast/{cast_id}','CastController@show');
    Route::get('/cast/{cast_id}/edit','CastController@edit');
    Route::put('/cast/{cast_id}','CastController@update');
    Route::delete('/cast/{cast_id}','CastController@destroy');
   
});








Auth::routes();


