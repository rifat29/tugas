<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peran extends Model
{
    
    protected $table = "peran";
    protected $fillable = ["films_id","casts_id","nama"];

    
    public function film(){
        return $this->belongsTo('App\film');
      }
    public function cast(){
        return $this->belongsTo('App\Cast');
      }
      
}
