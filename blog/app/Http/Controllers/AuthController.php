<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('Register');
    }

    public function kirim(Request $request){
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        return view('Home', compact('namadepan','namabelakang'));

    }
}
