<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = "films";
    protected $fillable = [ "judul","ringkasan","poster",'genre_id'];

    public function genre(){
        return $this->hasMany('App\genre');
      }

      public function kritik(){
        return $this->hasMany('App\kritik');
      }
      public function peran(){
        return $this->hasMany('App\peran');
      }
}
