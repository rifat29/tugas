<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $table = "genre";
    protected $fillable = [ "nama"];

    public function films(){
        return $this->belongsTo('App\film');
      }
}
