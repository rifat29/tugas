@extends('layout.master')

@section('judul')
    Halaman Update
@endsection

@section('content')
<h2>Tambah Data</h2>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Biodata</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="" placeholder="Masukkan Biodata">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">edit</button>
</form>
@endsection