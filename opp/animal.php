<?php

require_once "index.php";
require_once "ape.php";
require_once "frog.php";
$sheep = new Animal("shaun");

echo " nama: ". $sheep->name."<br>"; // "shaun"
echo " legs: ". $sheep->legs."<br>"; // 4
echo " cold blooded: : ". $sheep->cold_blooded."<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo " nama: ". $sungokong->name."<br>"; // "shaun"
echo " legs: ". $sungokong->legs."<br>"; // 4
echo " cold blooded: : ". $sungokong->cold_blooded."<br>"; // "no"
echo $sungokong->yell()."<br><br>"; // "Auooo"

$kodok = new Frog("buduk");
echo " nama: ". $kodok->name."<br>"; // "shaun"
echo " legs: ". $kodok->legs."<br>"; // 4
echo " cold blooded: : ". $kodok->cold_blooded."<br>"; // "no"
echo  $kodok->jump()."<br>"; // "Auooo"



?>